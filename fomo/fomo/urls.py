from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    # urls for any third-party apps go here

    #the built-in Django administrator
    url(r'^admin/', admin.site.urls),

    # the DMP router - this should be the last line in the list
    url('', include('django_mako_plus.urls')),

    ]
