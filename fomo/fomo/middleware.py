from catalog.models import Product

def Last5ProductsMiddleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        print ('>>>>>>>>>>>>>>> Beginning of Middleware function' )
        if request.session.get('last5') is not None:
            last5_products = request.session.get('last5')
        else:
            last5_products = []
        for id in last5_products:
            last5_products.append(Product.objects.get(id=id))

        request.last5 = last5_products

        #Django will call your view function here
        response = get_response(request)


        # Code to be executed for each request/response after
        # the view is called.
        if request.last5 is None:
            request.last5 = []
        last5_ids = []
        for product in request.last5[:6]:
            last5_ids.append(product.id)
        request.session['last5'] = last5_ids

        return response

    return middleware
