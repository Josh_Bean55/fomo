from django.conf import settings
from django_mako_plus import view_function
from django.contrib.auth.decorators import login_required, permission_required
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django.core.mail import send_mail
from datetime import datetime
from formlib.form import FormMixIn
from .. import dmp_render, dmp_render_to_string

@view_function
# @login_required(login_url='/account/login/')
def process_request(request):
    print('############################################## PROCESS REQUEST.CONTACT.PY')
    form = ContactForm(request)
    if form.is_valid():
        form.commit()
        return HttpResponseRedirect('/')

    #render the template
    return dmp_render(request, 'contact.html', {
        'form': form,
    })

class ContactForm(FormMixIn, forms.Form):
    SUBJECT_CHOICES = [
        ['payment', 'Payment issue'],
        ['upset', 'I am upset about something'],
        ['technical', 'I have a technical issue'],
        ['login', 'I have a login issue'],
        ['other', 'I have an other issue'],
    ]

    def init(self):
        self.fields['name'] = forms.CharField(label='Name', max_length=100)
        self.fields['contacttype'] = forms.ChoiceField(label='How can we contact you?', choices=[
            ['phone', 'Phone Number'],
            ['email', 'Email'],
        ])
        self.fields['phone'] = forms.CharField(label="Phone", required=False, max_length=100, widget=forms.TextInput(attrs={ 'class': 'contacttype-phone'}))
        # self.fields['cellnumber'] = forms.CharField(label="Cell", required=False, max_length=100, widget=forms.TextInput(attrs={ 'class': 'contacttype-phone'}))
        self.fields['email'] = forms.EmailField(label="Email", required=False, max_length=100, widget=forms.TextInput(attrs={ 'class': 'contacttype-email'}))
        self.fields['subject'] = forms.ChoiceField(label='Subject', choices=ContactForm.SUBJECT_CHOICES)
        self.fields['message'] = forms.CharField(label='Message', max_length=1000, widget=forms.Textarea())

    def clean_name(self):
        name = self.cleaned_data.get('name')
        #do the validation
        parts = name.split()
        if len(parts) <= 1:
            raise forms.ValidationError("Please give your first and last name")
        #return the value
        return name

    def commit(self):
        pass
        #if all checks out, then do work (contact -> send an email)
        #email = form.cleaned_data.get('email')
        #print('>>>>>>> sending the mail')
        #send mail(.....)
        #if self.request.is_superuser:
            #print('>>>>>>> is super')
            #do some specific logic
