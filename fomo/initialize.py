#Imports for my project
from django.core import management
from django.db import connection
from datetime import datetime
import os, os.path, sys


# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()


# drop and recreate the database tables
with connection.cursor() as cursor:
     cursor.execute("DROP SCHEMA public CASCADE")
     cursor.execute("CREATE SCHEMA public")
     cursor.execute("GRANT ALL ON SCHEMA public TO postgres")
     cursor.execute("GRANT ALL ON SCHEMA public TO public")

# make the migrations and migrate
management.call_command('makemigrations')
management.call_command('migrate')

#Imports for my project
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from account import models as amod
from catalog import models as cmod
import requests
import json

from decimal import Decimal



#PERMISSION CREATION
print('>>>>>>> Creating New Permission 1')
content_type =ContentType.objects.get_for_model(amod.FomoUser)
permissions = Permission.objects.create(
    codename = 'View_products',
    name = 'Can view list of products',
    content_type = content_type,
    )

print('>>>>>>> Creating New Permission 2')
content_type =ContentType.objects.get_for_model(amod.FomoUser)
permissions = Permission.objects.create(
    codename = 'View_users',
    name = 'Can view list of users',
    content_type = content_type,
    )


#Group Creation
print('>>>>>>> Creating New Group 1')
g1 = Group()
g1.name = 'Manager'
g1.save()

print('>>>>>>> Creating New Group 2')
g2 = Group()
g2.name = 'Salespeople'
g2.save()

print('>>>>>>> Creating New Group 3')
g3 = Group()
g3.name = 'Customer'
g3.save()

#Permissions Allocation
print('>>>>>>> Add Permissions to Group 1')
for p in Permission.objects.all():
    g1.permissions.add(p)
g1.permissions.add(Permission.objects.get(codename=('add_fomouser')))
g1.permissions.add(Permission.objects.get(codename=('change_fomouser')))
g1.permissions.add(Permission.objects.get(codename=('delete_fomouser')))
g1.permissions.add(Permission.objects.get(codename=('add_product')))
g1.permissions.add(Permission.objects.get(codename=('change_product')))
g1.permissions.add(Permission.objects.get(codename=('delete_product')))
g1.permissions.add(Permission.objects.get(codename=('View_products')))
g1.permissions.add(Permission.objects.get(codename=('View_users')))

print('>>>>>>> Add Permissions to Group 2')
g2.permissions.add(Permission.objects.get(codename=('View_products')))
g2.permissions.add(Permission.objects.get(codename=('View_users')))
g2.permissions.add(Permission.objects.get(codename=('change_fomouser')))


print('>>>>>>> Add Permissions to Group 3')
g3.permissions.add(Permission.objects.get(codename=('change_fomouser')))

# User object
print('>>>>>>> Creating User Object 1')
u1 = amod.FomoUser()
u1.username = 'bean22'
u1.first_name = 'Josh'
u1.last_name = 'Bean'
u1.email = 'bean@email.com'
u1.set_password('password1')
u1.phone = "222-232-5434"
u1.address = '12345 Center Avenue'
u1.city = 'Provo'
u1.state = 'UT'
u1.postal = '84606'
u1.gender = 'male'
u1.is_staff = True
u1.is_admin = True
u1.is_superuser = True
u1.save()

u1.groups.add(g1)

p = Permission.objects.get(codename='add_fomouser')
u1.user_permissions.add(p)


# User object
print('>>>>>>> Creating User Object 2')
u2 = amod.FomoUser()
u2.username = 'bmack'
u2.first_name = 'MacKenzie'
u2.last_name = 'Bowman'
u2.email = 'bowman@email.com'
u2.set_password('password')
u2.phone = "242-212-1234"
u2.address = '123 Center Street'
u2.city = 'Provo'
u2.state = 'UT'
u2.postal = '84606'
u2.gender = 'female'
u2.save()

u2.groups.add(g2)



print('>>>>>>> Creating User Object 3')
u3 = amod.FomoUser()
u3.username = 'alfie55'
u3.first_name = 'Alfredo'
u3.last_name = 'Olsen'
u3.email = 'olsen@email.com'
u3.set_password('password2')
u3.phone = "555-222-5444"
u3.address = '123456 Center Circle'
u3.city = 'Provo'
u3.state = 'UT'
u3.postal = '84606'
u3.gender = 'male'
u3.save()

u3.groups.add(g3)


print('>>>>>>> Creating User Object 4')
u4 = amod.FomoUser()
u4.username = 'dena'
u4.first_name = 'Deana'
u4.last_name = 'Mugimu'
u4.email = 'mugimu@email.com'
u4.set_password('password3')
u4.phone = "234-754-0000"
u4.address = '0987 Center Lane'
u4.city = 'Provo'
u4.state = 'UT'
u4.postal = '84606'
u4.gender = 'female'
u4.save()

u4.groups.add(g2)



print('>>>>>>> Creating User Object 5')
u5 = amod.FomoUser()
u5.username = 'Mr. Snel'
u5.first_name = 'Lex'
u5.last_name = 'Luther'
u5.email = 'snelson@email.com'
u5.set_password('password4')
u5.phone = "000-111-2222"
u5.address = '67548 Center Way'
u5.city = 'Provo'
u5.state = 'UT'
u5.postal = '84606'
u5.gender = 'male'
u5.save()

u5.groups.add(g3)



#Create Category objects
print('>>>>>>> Creating Catergory Object 1')
cat1 = cmod.Category()
cat1.codename = "kids"
cat1.name = "Kids Toy Products"
cat1.save()

print('>>>>>>> Creating Catergory Object 2')
cat2 = cmod.Category()
cat2.codename = "string"
cat2.name = "String Instruments"
cat2.save()

print('>>>>>>> Creating Catergory Object 3')
cat3 = cmod.Category()
cat3.codename = "brass"
cat3.name = "Brass Instruments"
cat3.save()

print('>>>>>>> Creating Catergory Object 4')
cat4 = cmod.Category()
cat4.codename = "woodwind"
cat4.name = "Woodwind Instruments"
cat4.save()

print('>>>>>>> Creating Catergory Object 5')
cat5 = cmod.Category()
cat5.codename = "accessory"
cat5.name = "Music Accessories"
cat5.save()

#Create Bulk Product Objects
print('>>>>>>> Creating Bulk Product Object 1')
bp1 = cmod.BulkProduct()
bp1.name = "Harmonica"
bp1.category = cat1
bp1.price = Decimal('9.50')
bp1.quantity = 20
bp1.reorder_trigger = 5
bp1.reorder_quantity = 30
bp1.description = "Imported phosphor bronze reed(Sound Spring):Copper content 99.99%,anticorrosion,antifatigue with brighter tone. Germany equipment:The Sino-German joint venture DMG CNC planer spring machine,guarantee the sound spring intonation. Thicken Copper plate,1mm copper plate makes the harmonica sound more clear and melodious. Harmonica Cover design:thicken copper cover,fine printing design with enough hardness,it is not easy to deform with long time playing."
bp1.save()

pPic1 = cmod.ProductPicture()
pPic1.filepath = '/static/catalog/media/harmonica.svg'
pPic1.product = bp1
pPic1.save()

pPic1 = cmod.ProductPicture()
pPic1.filepath = '/static/catalog/media/product_images/harmonica1.jpg'
pPic1.product = bp1
pPic1.save()

pPic1 = cmod.ProductPicture()
pPic1.filepath = '/static/catalog/media/product_images/harmonica2.jpg'
pPic1.product = bp1
pPic1.save()

pPic1 = cmod.ProductPicture()
pPic1.filepath = '/static/catalog/media/product_images/harmonica3.jpg'
pPic1.product = bp1
pPic1.save()

pPic1 = cmod.ProductPicture()
pPic1.filepath = '/static/catalog/media/product_images/harmonica4.jpg'
pPic1.product = bp1
pPic1.save()

print('>>>>>>> Creating Bulk Product Object 2')
bp2 = cmod.BulkProduct()
bp2.name = "Earphones"
bp2.category = cat5
bp2.price = Decimal('12.75')
bp2.quantity = 10
bp2.reorder_trigger = 5
bp2.reorder_quantity = 20
bp2.description = "COMFORTABLE AND SECURE FIT: With detachable ear-hook and comfort-fit in-ear earbuds, it will rest comfortably in the ear canal and should not fall out during intense outworks. They are smaller than other in-ear headphones and comes with adjustable S/M/L earbuds. PREMIUM LIGHTWEIGHT HEADPHONES: Optimized ergonomic auricle design can have comfortable wearing in the ears. These exercise earbuds are 15g lightweight with soft wire wrapped around the ears. "
bp2.save()

pPic2 = cmod.ProductPicture()
pPic2.filepath = '/static/catalog/media/earphones.svg'
pPic2.product = bp2
pPic2.save()

pPic2 = cmod.ProductPicture()
pPic2.filepath = '/static/catalog/media/product_images/earphones1.jpg'
pPic2.product = bp2
pPic2.save()

pPic2 = cmod.ProductPicture()
pPic2.filepath = '/static/catalog/media/product_images/earphones2.jpg'
pPic2.product = bp2
pPic2.save()

pPic2 = cmod.ProductPicture()
pPic2.filepath = '/static/catalog/media/product_images/earphones3.jpg'
pPic2.product = bp2
pPic2.save()

pPic2 = cmod.ProductPicture()
pPic2.filepath = '/static/catalog/media/product_images/earphones4.jpg'
pPic2.product = bp2
pPic2.save()

print('>>>>>>> Creating Unique Product Object 1')
up1 = cmod.UniqueProduct()
up1.name = "Trumpet"
up1.category = cat3
up1.price = Decimal('200.00')
up1.serial_number = "12345-ABCDE"
up1.description = "Gold lacquered Bb trumpet with 7C mouthpiece; 0.46 inch bore & 5 inch bell, 1st valve slide thumb saddle, 3rd valve slide with adjustable throw ring; Phosphorus copper used on lead mouth pipe (found on expensive trumpets), smooth action valves, & comfortable white faux mother of pearl inlaid buttons"
up1.save()


pPic3 = cmod.ProductPicture()
pPic3.filepath = '/static/catalog/media/trumpet.svg'
pPic3.product = up1
pPic3.save()

pPic3 = cmod.ProductPicture()
pPic3.filepath = '/static/catalog/media/product_images/trumpet1.jpg'
pPic3.product = up1
pPic3.save()

pPic3 = cmod.ProductPicture()
pPic3.filepath = '/static/catalog/media/product_images/trumpet2.jpg'
pPic3.product = up1
pPic3.save()

pPic3 = cmod.ProductPicture()
pPic3.filepath = '/static/catalog/media/product_images/trumpet3.jpg'
pPic3.product = up1
pPic3.save()

pPic3 = cmod.ProductPicture()
pPic3.filepath = '/static/catalog/media/product_images/trumpet4.jpg'
pPic3.product = up1
pPic3.save()

print('>>>>>>> Creating Unique Product Object 2')
up2 = cmod.UniqueProduct()
up2.name = "Banjo"
up2.category = cat2
up2.price = Decimal('500.00')
up2.serial_number = "93649-BCYTS"
up2.description = "Mahogany Body^Remo 11 Clear Head^Bolt-On Neck^Rosewood Fingerboard^Chrome Hardware"
up2.save()

pPic4 = cmod.ProductPicture()
pPic4.filepath = '/static/catalog/media/banjo.svg'
pPic4.product = up2
pPic4.save()

pPic4 = cmod.ProductPicture()
pPic4.filepath = '/static/catalog/media/product_images/banjo1.jpg'
pPic4.product = up2
pPic4.save()

pPic4 = cmod.ProductPicture()
pPic4.filepath = '/static/catalog/media/product_images/banjo2.jpg'
pPic4.product = up2
pPic4.save()

pPic4 = cmod.ProductPicture()
pPic4.filepath = '/static/catalog/media/product_images/banjo3.jpg'
pPic4.product = up2
pPic4.save()

pPic4 = cmod.ProductPicture()
pPic4.filepath = '/static/catalog/media/product_images/banjo4.jpg'
pPic4.product = up2
pPic4.save()

print('>>>>>>> Creating Unique Product Object 3')
up3 = cmod.UniqueProduct()
up3.name = "Electric Guitar"
up3.category = cat2
up3.price = Decimal('425.00')
up3.serial_number = "08940-MAQPZ"
up3.description = "Maple Jointed Neck; Medium Jumbo Nickel Alloy Frets, 22 Fret Positions, 25.5 Fret Scale, 43mm nut width, Black ABS Dot Inlays, Hand Contured ULTRALIGHT Body"
up3.save()

pPic5 = cmod.ProductPicture()
pPic5.filepath = '/static/catalog/media/electric-guitar.svg'
pPic5.product = up3
pPic5.save()

pPic5 = cmod.ProductPicture()
pPic5.filepath = '/static/catalog/media/product_images/eguitar1.jpg'
pPic5.product = up3
pPic5.save()

pPic5 = cmod.ProductPicture()
pPic5.filepath = '/static/catalog/media/product_images/eguitar2.jpg'
pPic5.product = up3
pPic5.save()

pPic5 = cmod.ProductPicture()
pPic5.filepath = '/static/catalog/media/product_images/eguitar3.jpg'
pPic5.product = up3
pPic5.save()

pPic5 = cmod.ProductPicture()
pPic5.filepath = '/static/catalog/media/product_images/eguitar4.jpg'
pPic5.product = up3
pPic5.save()

print('>>>>>>> Creating Rental Product Object 1')
rp1 = cmod.RentalProduct()
rp1.name = "Clarinet Rental"
rp1.category = cat4
rp1.price = Decimal('425.00')
rp1.serial_number = "00002-IOPQA"
rp1.description = "We are a USA Brand that you can trust 100%. Our clarinets are not just stylish, but also very durable, have great sound quality and are always hand checked by our professional team in New York."
rp1.save()

pPic6 = cmod.ProductPicture()
pPic6.filepath = '/static/catalog/media/clarinet.svg'
pPic6.product = rp1
pPic6.save()

pPic6 = cmod.ProductPicture()
pPic6.filepath = '/static/catalog/media/product_images/clarinet1.jpg'
pPic6.product = rp1
pPic6.save()

pPic6 = cmod.ProductPicture()
pPic6.filepath = '/static/catalog/media/product_images/clarinet2.jpg'
pPic6.product = rp1
pPic6.save()

pPic6 = cmod.ProductPicture()
pPic6.filepath = '/static/catalog/media/product_images/clarinet3.jpg'
pPic6.product = rp1
pPic6.save()

pPic6 = cmod.ProductPicture()
pPic6.filepath = '/static/catalog/media/product_images/clarinet4.jpg'
pPic6.product = rp1
pPic6.save()


print('>>>>>>> Creating Rental Product Object 2')
rp2 = cmod.RentalProduct()
rp2.name = "Flute Rental"
rp2.category = cat4
rp2.price = Decimal('500.00')
rp2.serial_number = "73418-MSVZO"
rp2.description = "Beautiful Flute with Lacquer Keys, Made from Real CUPRONICKEL, Closed Hole 16 Keys Design, Key of C with C Footjoint, Offset G, Split E Mechanism, High Quality Pads Imported from Italy, Precision Drawn Tone Holes."
rp2.save()

pPic7 = cmod.ProductPicture()
pPic7.filepath = '/static/catalog/media/flute.svg'
pPic7.product = rp2
pPic7.save()

pPic7 = cmod.ProductPicture()
pPic7.filepath = '/static/catalog/media/product_images/flute1.jpg'
pPic7.product = rp2
pPic7.save()

pPic7 = cmod.ProductPicture()
pPic7.filepath = '/static/catalog/media/product_images/flute2.jpg'
pPic7.product = rp2
pPic7.save()

pPic7 = cmod.ProductPicture()
pPic7.filepath = '/static/catalog/media/product_images/flute3.jpg'
pPic7.product = rp2
pPic7.save()

pPic7 = cmod.ProductPicture()
pPic7.filepath = '/static/catalog/media/product_images/flute4.jpg'
pPic7.product = rp2
pPic7.save()


print('>>>>>>> Creating Rental Product Object 3')
rp3 = cmod.RentalProduct()
rp3.name = "Violin Rental"
rp3.category = cat2
rp3.price = Decimal('350.00')
rp3.serial_number = "73418-MSVZO"
rp3.description = "4/4 (Full Size) Acoustic & Electric Violin; Hand-carved solid spruce top, solid maple back & sides with volume and tone control; Ebony pegs, chin rest and fingerboard, tailpiece with 4 nickel plated fine tuners; Includes: Brazilwood bow with unbleached genuine Mongolian horsehair, Lightweight foam fitting hard case, Adjustable shoulder rest with soft foam padding with soft rubber feet, Violin bridge, High quality rosin, and an AUX cable; 1 year warranty against manufacturer's defects"
rp3.save()

pPic8 = cmod.ProductPicture()
pPic8.filepath = '/static/catalog/media/violin.svg'
pPic8.product = rp3
pPic8.save()

pPic8 = cmod.ProductPicture()
pPic8.filepath = '/static/catalog/media/product_images/violin1.jpg'
pPic8.product = rp3
pPic8.save()

pPic8 = cmod.ProductPicture()
pPic8.filepath = '/static/catalog/media/product_images/violin2.jpg'
pPic8.product = rp3
pPic8.save()

pPic8 = cmod.ProductPicture()
pPic8.filepath = '/static/catalog/media/product_images/violin3.jpg'
pPic8.product = rp3
pPic8.save()

pPic8 = cmod.ProductPicture()
pPic8.filepath = '/static/catalog/media/product_images/violin4.jpg'
pPic8.product = rp3
pPic8.save()

#Creating objects in the shopping cart
print('>>>>>>> Creating Shopping Cart Item Object 1')
sc1 = cmod.ShoppingCartItem()
sc1.user = u1
sc1.product = bp1
sc1.quantity = 2
sc1.is_active = True
sc1.save()

print('>>>>>>> Creating Shopping Cart Item Object 2')
sc2 = cmod.ShoppingCartItem()
sc2.user = u1
sc2.product = up3
sc2.quantity = 1
sc2.is_active = True
sc2.save()

print('>>>>>>> Creating Shopping Cart Item Object 3')
sc3 = cmod.ShoppingCartItem()
sc3.user = u1
sc3.product = up2
sc3.quantity = 1
sc3.is_active = True
sc3.save()



#print('\n>>>Beginning of API Test')
#for r in(
#'http://localhost:8000/api/products?product_name=clarinet',
#'http://localhost:8000/api/products?max_price=tuba',
#'http://localhost:8000/api/products?product_name=&category_name=b&max_price100&min_price=30',
#'http://localhost:8000/api/products?product_name=flute&category_name=b&max_price100&min_price=30',
#):
    #req = requests.get(r)
    #print('\nTest url: ', r)
    #print('\tResponse: ', req.text)

#This indicates that I made it to the bottom of my file
#print('>>>>>>> Congrats big boy, you win')
