from django.conf import settings
from django_mako_plus import view_function
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required, permission_required
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from datetime import datetime
from formlib.form import FormMixIn
from .. import dmp_render, dmp_render_to_string
import ldap3
import json


@view_function
def process_request(request):
    #process the form
    print('############################################## PROCESS REQUEST LOGIN.PY')
    form = LoginForm(request)
    if form.is_valid():
        form.commit()
        #redirect to the my account page
        return HttpResponseRedirect('/homepage/index/')

    #if not authenticated
    return dmp_render(request, 'login.html', {
        'form': form,
    })


class LoginForm(FormMixIn, forms.Form):
    #The Login Form

    def init(self):
        #Initializes the form fields
        #fields
        self.fields['username'] = forms.CharField(required=True)
        self.fields['password'] = forms.CharField(required=True, widget=forms.PasswordInput())

    def clean(self):
        #Authenticate username and password
        self.user = authenticate(username=self.cleaned_data.get('username'), password=self.cleaned_data.get('password'))
        if self.user is None:
            raise forms.ValidationError('Invalid username or password')

        #always return the cleaned data
        return self.cleaned_data

    def commit(self):
        login(self.request, self.user)


##############################################################################################################################
@view_function
def modal(request):
    print('############################################## PROCESS REQUEST LOGIN.Moday')
    #process the form
    form = ModalLoginForm(request)
    if form.is_valid():
        form.commit()
        #redirect to the my account page


        return HttpResponse('''

        <script>
            window.location.href = '/homepage/index/'
        </script>

        ''')

    #if not authenticated
    return dmp_render(request, 'login.modal.html', {
        'form': form,
    })


class ModalLoginForm(LoginForm):
    form_action = '/account/login.modal/'

    #authenticate the user
    #username = "bean22"
    #password = "password1"
    #user = authenticate(username=username, password=password)

    #log the user in
    #if user is not None:
        #login(request, user)

        #go to the my homepage page
        #return HttpResponseRedirect('/homepage/index/')

    #return HttpResponseRedirect('/homepage/index/')
