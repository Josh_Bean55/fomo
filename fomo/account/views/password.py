from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from datetime import datetime
from django import forms
from account import models as amod
from formlib.form import FormMixIn
from django.contrib.auth import authenticate, login
from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):
    try:
        user = request.user
    except amod.FomoUser.DoesNotExist:
        return HttpResponseRedirect('/index/')
    form = UserEditForm(request, user=user, initial = {
        'username':user.username,
    })

    if form.is_valid():
        uname = form.cleaned_data.get('username')
        p1 = form.cleaned_data.get('Password1')
        p2 = form.cleaned_data.get('Password2')
        p3 = form.cleaned_data.get('Password3')
        if p2 == p3:
            user = authenticate(username=uname, password=p1)
            if user is not None:
                form.commit(user)
                login(request, user)
                return HttpResponseRedirect('/catalog/index')
            else:
                return HttpResponseRedirect('/account/login')
        return HttpResponseRedirect('index')
    context = {
        'user':user,
        'form':form,
    }
    return dmp_render(request, 'password.html', context)

class UserEditForm(FormMixIn, forms.Form):
    def init(self, user):
        self.fields['username'] = forms.CharField(label="Username", max_length=100)
        self.fields['Password1'] = forms.CharField(label="Current Password", required=True, widget=forms.PasswordInput())
        self.fields['Password2'] = forms.CharField(label="New Password", required=True, widget=forms.PasswordInput())
        self.fields['Password3'] = forms.CharField(label="Retype New Password", required=True, widget=forms.PasswordInput())
    def commit(self,user):
        print('>>>>That worked')
        user.set_password(self.cleaned_data.get('Password2'))
        user.save()
