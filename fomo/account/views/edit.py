from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from account import models as amod
from django.http import HttpResponse, HttpResponseRedirect
from django import forms
from formlib.form import FormMixIn
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required


@view_function
@login_required(login_url='/account/login/')
def process_request(request):
    user = amod.FomoUser.objects.get(id=request.user.id)

    form = Edituser(request, initial ={
        'username': user.username,
        'first_name': user.first_name,
        'last_name' : user.last_name,
        'gender' : user.gender,
        'address': user.address,
        'postal': user.postal,
        'email': user.email,
        'phone': user.phone,
        'city': user.city,
        'state': user.state,
    })

    if form.is_valid():
        form.commit(user)
        return HttpResponseRedirect('/catalog/index')

    return dmp_render(request, 'edit.html', {'form' :form, })


class Edituser(FormMixIn, forms.Form):

        def init(self):
            self.fields['username'] = forms.CharField(label='Username', max_length=100)
            self.fields['first_name'] = forms.CharField(label='First Name')
            self.fields['last_name'] = forms.CharField(label='Last Name')
            self.fields['gender'] = forms.ChoiceField(label="Gender", choices=[
                ['male', 'Male'],
                ['female', 'Female'],
            ])
            self.fields['address'] = forms.CharField(label='Address')
            self.fields['postal'] = forms.CharField(label='Postal Code')
            self.fields['email'] = forms.EmailField(label='Email')
            self.fields['phone'] = forms.CharField(label='Phone Number')
            self.fields['city'] = forms.CharField(label='City')
            self.fields['state'] = forms.CharField(label='State')

        def clean_username(self):
            un = self.cleaned_data.get('username')
            users = amod.FomoUser.objects.filter(username = un).exclude(id = self.request.user.id)

            if len(users) > 0:
                raise forms.ValidationError('This username is taken')

            return un

        def commit(self, user):
            user.username = self.cleaned_data.get('username')
            user.first_name = self.cleaned_data.get('first_name')
            user.last_name = self.cleaned_data.get('last_name')
            user.gender = self.cleaned_data.get('gender')
            user.address = self.cleaned_data.get('address')
            user.postal = self.cleaned_data.get('postal')
            user.email = self.cleaned_data.get('email')
            user.phone = self.cleaned_data.get('phone')
            user.city = self.cleaned_data.get('city')
            user.state = self.cleaned_data.get('state')
            user.save()
