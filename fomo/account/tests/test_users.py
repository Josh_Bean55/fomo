from django.test import TestCase
from account import models as amod

class CreateUserTestCase(TestCase):

    def test_create_a_user(self):
        #setUp is called automatically
        u1 = amod.FomoUser()
        u1.first_name = 'Mike'
        u1.last_name = 'W.'
        u1.username = "MikeyBoo"
        u1.email = 'mike@monster.com'
        u1.save()

        u2 = amod.FomoUser.objects.get(id=u1.id)
        self.assertEquals(u2.first_name, 'Mike')
        self.assertEquals(u2.last_name, 'W.')
        self.assertEquals(u2.username, "MikeyBoo")
        self.assertEquals(u2.email, 'mike@monster.com')
