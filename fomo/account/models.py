# Account APP
from django.db import models
from django.contrib.auth.models import AbstractUser
from catalog import models as cmod
from decimal import Decimal

#This is my user class
class FomoUser(AbstractUser):
    #Inheriting from AbstractBaseUser
        #password
        #last_login
    #Inheriting from AbstractUser
        #username
        #first_name
        #last_name
        #email
        #date_joined
     gender = models.TextField(null=True)
     address = models.TextField()
     city = models.TextField()
     state = models.TextField()
     postal = models.TextField()
     phone = models.TextField()

     #This method acquires the last five items searched based on the user
     def get_last_five(self):
        last_viewed = []
        for vh in cmod.ViewHistory.objects.filter(user_id=self.id).order_by('-date_viewed'):
            if vh.product not in last_viewed:
                last_viewed.append(vh.product)
            if len(last_viewed) >= 5:
                break
        return last_viewed

     #This method is used to calculate the number of items in the shopping cart
     def item_count(self):
        item_count = cmod.ShoppingCartItem.objects.filter(user_id = self.id, is_active = True).count()
        return item_count

     #This method is used to empty out the cart completely
     def clear_cart(self):
         products = cmod.ShoppingCartItem.objects.filter(user_id=self.id).filter(is_active=True).update(is_active=False)

    #This method is used to retrieve all of the contents of the cart
     def get_cart(self):
         products = cmod.ShoppingCartItem.objects.filter(user_id=self.id).filter(is_active=True)
         return products

     def add_to_cart(self, form):
         cart_item = self.user_shoppingcartitem.filter(product=form.product).filter(is_active=True)
         if cart_item.exists():
             print('item is in cart')
             cart_item = cart_item.get(product=form.product)
             if hasattr(form.product, 'quantity'):
                 cart_item.quantity += form.cleaned_data.get('quantity')
                 cart_item.save()
         else:
             print('item is not in cart')
             new_item = cmod.ShoppingCartItem()
             new_item.user = form.user
             new_item.product = form.product
             print(hasattr(form.product, 'quantity'))
             if hasattr(form.product, 'quantity'):
                 new_item.quantity = form.cleaned_data.get('quantity')
             else:
                 new_item.quantity = 1
             new_item.save()

    #This method is used to calculate the base total of the products being purchased
     def calc_subtotal(self):
         products = cmod.ShoppingCartItem.objects.filter(user_id=self.id).filter(is_active=True)
         subtotal = 0

         for p in products:
             subtotal = subtotal + (p.product.price * p.quantity)

         return subtotal


    #This method is used to calculate taxes for all items
     def calc_total_tax(self):
         total_tax = 0
         products = cmod.ShoppingCartItem.objects.filter(user_id=self.id).filter(is_active=True)
         tax_rate = Decimal(0.0725)

         for p in products:
             total_tax = total_tax + Decimal(p.quantity) * Decimal(p.product.price) * tax_rate
         return round(total_tax, 2)

     #This method is used to calculate the shipping cost, it will be a fixed cost of $10.00
     def calc_shipping(self):
         shipping = Decimal('10.00')
         return shipping

     #This method is used to calculate the total, based off the sub-total, taxes, and shipping costs
     def calc_total(self):
         total = self.calc_subtotal() + self.calc_total_tax() + self.calc_shipping()
         return total
