from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from datetime import datetime
from account import models as amod
from formlib.form import FormMixIn
from django import forms
from .. import dmp_render, dmp_render_to_string
from django.contrib.auth.models import Permission, Group
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required

#######################################################################################

@view_function
def create(request):

    form = UserEditForm(request)

    if form.is_valid():
        user = amod.FomoUser()
        form.commit(user)
        return HttpResponseRedirect('/homepage/index/')
    return dmp_render(request, 'users.html',{'form' :form,})

#########################################################################################

@view_function
@permission_required('change_fomouser', login_url='/account/edit_user/')
@login_required(login_url='/account/login/')

def process_request(request):

    try:
        user = amod.FomoUser.objects.get(id=request.urlparams[0])
    except amod.FomoUser.DoesNotExist:
        return HttpResponseRedirect('/manager/users/')

    # process the form
    form = UserEditForm(request, user=user, initial={
        'last_name': user.last_name,
        'first_name': user.first_name,
        'username': user.username,
        'email': user.email,
    })
    if form.is_valid():
        form.commit(user)
        return HttpResponseRedirect('/manager/users/')

    context = {
        'user': user,
        'form': form,
    }
    return dmp_render(request, 'edit_user.html', context)


class UserEditForm(FormMixIn, forms.Form):

    def init(self, user):
        # self.fields['username'] = forms.CharField(label='Username', max_length=100)
        self.fields['first_name'] = forms.CharField(label="First Name", max_length=100)
        self.fields['last_name'] = forms.CharField(label="Last Name", max_length=100)
        # self.fields['gender'] = forms.ChoiceField(label="Gender", choices=[
        #     ['male', 'Male'],
        #     ['female', 'Female'],
        # ])
        self.fields['email'] = forms.EmailField(label="email", max_length=100)
        self.fields['groups'] = forms.ModelMultipleChoiceField(label='Group',
        queryset=Group.objects.order_by('name').all(), required=False)
        self.fields['permissions'] = forms.ModelMultipleChoiceField(label="Permissions", required=False,
        queryset=Permission.objects.all(), help_text='Hold control to select multiple permissions.')
        self.fields['address'] = forms.CharField(label='Address')
        # self.fields['postal'] = forms.CharField(label='Postal')
        self.fields['phone'] = forms.CharField(label='Phone Number')
        self.fields['city'] = forms.CharField(label='City')
        self.fields['state'] = forms.CharField(label='State')
        # self.fields['birthday'] = forms.DateField(required = True)

    def clean_username(self):
        un = self.cleaned_data.get('username')
        users = amod.FomoUser.objects.filter(username = un).exclude(id = self.request.urlparams[0])

        if len(users) > 0:
            raise forms.ValidationError('This username is taken')

        return un

    def commit(self, user):
        user.last_name = self.cleaned_data.get('last_name')
        user.first_name = self.cleaned_data.get('first_name')
        # user.gender = self.cleaned_data.get('gender')
        # user.username = self.cleaned_data.get('username')
        user.email = self.cleaned_data.get('email')
        user.address  = self.cleaned_data.get('address')
        # user.postal = self.cleaned_data.get('postal')
        user.phone = self.cleaned_data.get('phone')
        user.city = self.cleaned_data.get('city')
        user.state = self.cleaned_data.get('state')

        user.save()

        user.user_permissions.set(self.cleaned_data.get('permissions'))
        user.groups.set(self.cleaned_data.get('groups'))

###########################################################################################################
#This is used to delete users

@view_function
@permission_required('delete_fomouser', login_url='/account/delete_user/')
def delete(request):
    try:
        user = amod.FomoUser.objects.get(id=request.urlparams[0])
    except amod.FomoUser.DoesNotExist:
        return HttpResponseRedirect('/manager/users/')

    user.delete()
    return HttpResponseRedirect('/manager/users/')
