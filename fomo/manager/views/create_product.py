from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from datetime import datetime
from catalog import models as cmod
from formlib.form import FormMixIn
from django import forms
from .. import dmp_render, dmp_render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required

########################################################################################################
#This is used to create products

print('>>>>>>>>>>>>>>>>>>>>>>> DEEZ NUTS')
@view_function
@login_required(login_url='/account/login_class/')
@permission_required('catalog.create_product', login_url='/manager/products/')
def process_request(request):
    form = CreateForm(request)
    if form.is_valid():
        form.commit()
        return HttpResponseRedirect('/manager/products/')

    #render the template
    return dmp_render(request, 'create_product.html', {
        'form': form,
    })

print('>>>>>>>>>>>>>>>>>>>>>>>>>>Right above the Create Form')
class CreateForm(FormMixIn, forms.Form):
    def init(self, product):
        self.fields['type'] = forms.ChoiceField(label='Select the type of product:', choices=[
            ['unique', 'Unique Product'],
            ['bulk', 'Bulk Product'],
            ['rental', 'Rental Product'],
        ])
        self.fields['name'] = forms.CharField(label='Product Name', max_length=100)
        self.fields['category'] = forms.ModelChoiceField(label="Category", queryset=cmod.Category.objects.order_by('name').all())
        self.fields['price'] = forms.DecimalField(label="Price", max_digits=8, decimal_places=2) #999,999.99)
        self.fields['create_date'] = forms.DateTimeField(label="Date Created", required=False)
        self.fields['modified_date'] = forms.CharField(label="Date Modified", required=False)
        self.fields['quantity'] = forms.CharField(label="Quantity", required=False)
        self.fields['reorder_trigger'] = forms.CharField(label="Reorder Trigger", required=False)
        self.fields['reorder_quantity'] = forms.CharField(label="Reorder Quantity", required=False)
        self.fields['serial_number'] = forms.CharField(label="Serial Number", required=False)
        #self.fields['is_rental'] = forms.ChoiceField(label="Rental", required=False, choices=[
        #['yes', 'Yes'],
        #['no', 'No']
        #])

    def commit(self):

        try:
            print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> SUH DUDE')
            if self.cleaned_data.get('type') == 'bulk':
                print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BULK WORKS MAH NIGGGGAAAAAAA')
                product = cmod.BulkProduct(name=self.cleaned_data.get('name'),
                    category=self.cleaned_data.get('category'),
                    price=self.cleaned_data.get('price'),
                    quantity=self.cleaned_data.get('quantity'),
                    reorder_trigger=self.cleaned_data.get('reorder_trigger'),
                    reorder_quantity=self.cleaned_data.get('reorder_quantity')
                )
                product.save()
            if self.cleaned_data.get('type') == 'rental':
                print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> RENTAL PRDOCUT WORKS MAH NIGGA')
                product = cmod.RentalProduct(name=self.cleaned_data.get('name'),
                    category=self.cleaned_data.get('category'),
                    price=self.cleaned_data.get('price'),
                    serial_number=self.cleaned_data.get('serial_number'),
                )
                product.save()
            if self.cleaned_data.get('type') == 'unique':
                print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> UNIQUE PRDOUCT CREATION MAH NIGGA')
                product = cmod.UniqueProduct(name=self.cleaned_data.get('name'),
                    category=self.cleaned_data.get('category'),
                    price=self.cleaned_data.get('price'),
                    serial_number=self.cleaned_data.get('serial_number'),
                )
                product.save()
        except:
            print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> YOU BROKE IT YOU WANKER')
            return HttpResponseRedirect('/manager/products')
