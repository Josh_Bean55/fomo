from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from datetime import datetime
from account import models as amod
from .. import dmp_render, dmp_render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required

@view_function
@permission_required('account.View_users', login_url='/homepage/index/')
@login_required(login_url='/account/login/')
def process_request(request):
    #query all users
    users = amod.FomoUser.objects.order_by('last_name').all()
    print('>>>>>>>', users)

    context = {
        'users': users,
    }
    return dmp_render(request, 'users.html', context)
