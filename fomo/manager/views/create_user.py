from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from account import models as amod
from django.http import HttpResponse, HttpResponseRedirect
from django import forms
from formlib.form import FormMixIn
from django.contrib.auth.models import Permission, Group
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required

@view_function
@permission_required('add_fomouser', login_url='/account/create_user/')
@login_required(login_url='/account/login/')
def process_request(request):

    form = SignupForm(request)

    if form.is_valid():
        user = amod.FomoUser()
        form.commit(user)
        # login(request, form.new_user)
        return HttpResponseRedirect('/manager/users')


    return dmp_render(request, 'create_user.html', {'form' :form, })


class SignupForm(FormMixIn, forms.Form):
    def init(self, new_user):
        self.fields['username'] = forms.CharField(label='Username', max_length=100)
        self.fields['password'] = forms.CharField(max_length=32, widget=forms.PasswordInput)
        self.fields['first_name'] = forms.CharField(label='First Name')
        self.fields['last_name'] = forms.CharField(label='Last Name')
        self.fields['address'] = forms.CharField(label='Address')
        self.fields['postal'] = forms.CharField(label='Postal Code')
        self.fields['email'] = forms.EmailField(label='Email')
        self.fields['phone'] = forms.CharField(label='Phone Number')
        self.fields['city'] = forms.CharField(label='City')
        self.fields['state'] = forms.CharField(label='State')
        self.fields['birthday'] = forms.DateField(required = False)
        self.fields['groups'] = forms.ModelMultipleChoiceField(label='Group',
        queryset=Group.objects.order_by('name').all(), required=False)
        self.fields['permissions'] = forms.ModelMultipleChoiceField(label="Permissions", required=False,
        queryset=Permission.objects.all(), help_text='Hold control to select multiple permissions.')

    def clean_username(self):
        un = self.cleaned_data.get('username')
        users = amod.FomoUser.objects.filter(username = un).exclude(id = self.request.user.id)

        if len(users) > 0:
            raise forms.ValidationError('This username is taken')

        return un

    def commit(self, user):
        user.username = self.cleaned_data.get('username')
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name  = self.cleaned_data.get('last_name')
        user.address  = self.cleaned_data.get('address')
        user.zip = self.cleaned_data.get('zip')
        user.email = self.cleaned_data.get('email')
        user.phone_number = self.cleaned_data.get('phone_number')
        user.city = self.cleaned_data.get('city')
        user.state = self.cleaned_data.get('state')
        user.set_password(self.cleaned_data.get('password'))

        user.save()
        user.user_permissions.set(self.cleaned_data.get('permissions'))
        user.groups.set(self.cleaned_data.get('groups'))

















#from django.conf import settings
#from django.http import HttpResponse, HttpResponseRedirect
#from django_mako_plus import view_function
#from datetime import datetime
#from account import models as amod
#from formlib.form import FormMixIn
#from django import forms
#from .. import dmp_render, dmp_render_to_string
#from django.contrib.auth.models import Permission, Group


########################################################################################################
#This is used to create products

#print('>>>>>>>>>>>>>>>>>>>>>>> DEEZ NUTS')
#@view_function
#def process_request(request):
    #form = CreateUserForm(request)
    #if form.is_valid():
        #user = amod.FomoUser()
        #form.commit()
        #return HttpResponseRedirect('/manager/users/')

    #render the template
    #return dmp_render(request, 'create_user.html', {
        #'form': form,
    #})

#print('>>>>>>>>>>>>>>>>>>>>>>>>>>Right above the Create Form')
#class CreateUserForm(FormMixIn, forms.Form):
    #def init(self, user):
        #self.fields['username'] = forms.CharField(label="username", max_length=100, required=True)
        #self.fields['first_name'] = forms.CharField(label='First Name', max_length=100, required=True)
        #self.fields['last_name'] = forms.CharField(label="Last Name", max_length=100, required=True)
        #self.fields['gender'] = forms.ChoiceField(label="Gender", choices=[
            #['male', 'Male'],
            #['female', 'Female'],
        #])
        #self.fields['email'] = forms.CharField(label="Email", max_length=100, required=True)
        #self.fields['password'] = forms.CharField(label="Password", max_length=100, widget=forms.PasswordInput(), required=True)
        #self.fields['groups'] = forms.ModelMultipleChoiceField(label='Group',
        #queryset=Group.objects.order_by('name').all(), required=False)
        #self.fields['permissions'] = forms.ModelMultipleChoiceField(label="Permissions", required=False,
        #queryset=Permission.objects.all(), help_text='Hold control to select multiple permissions.')
        #self.fields['date_joined'] = forms.DateField(label="Date Joined", required=False)
        #self.fields['address'] = forms.CharField(label="Address", max_length=100, required=False)
        #self.fields['city'] = forms.CharField(label="City", max_length=100, required=False)
        #self.fields['state'] = forms.CharField(label="State", max_length=2, required=False)
        #self.fields['postal'] = forms.CharField(label="Postal", max_length=5, required=False)
        #self.fields['phone'] = forms.CharField(label="Phone", max_length=100, required=False)
        #self.fields['birthday'] = forms.DateField(required = True)




    #def commit(self):
        #user = amod.FomoUser(first_name=self.cleaned_data.get('first_name'),
            #last_name=self.cleaned_data.get('last_name'),
            #username=self.cleaned_data.get('username'),
            #password=self.cleaned_data.get('first_name'),
            #email=self.cleaned_data.get('email'),
            #gender=self.cleaned_data.get('gender'),
            #address=self.cleaned_data.get('address'),
            #city=self.cleaned_data.get('city'),
            #state=self.cleaned_data.get('state'),
            #postal=self.cleaned_data.get('postal'),
            #phone=self.cleaned_data.get('phone'),
            #)
        #user.save()

        #user.user_permissions.set(self.cleaned_data.get('permissions'))
        #user.groups.set(self.cleaned_data.get('groups'))
