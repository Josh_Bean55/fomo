from django.db import models
from decimal import Decimal
from polymorphic.models import PolymorphicModel

# Create your models here.
class Category(models.Model):
    #id
    codename = models.TextField(blank=True, null=True, unique=True)
    name = models.TextField(blank = True, null = True)

    def __str__(self):
        return self.name

class Product(PolymorphicModel):
    #id
    name = models.TextField(blank=True, null=True)
    category = models.ForeignKey('Category')
    price = models.DecimalField(max_digits=8, decimal_places=2) #999,999.99
    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    description = models.TextField(blank=True, null=True)
    is_purchased = models.TextField(default=False)

class ProductPicture(models.Model):
    product = models.ForeignKey('Product', related_name='pictures')
    filepath = models.TextField(blank=True, null=True)
    # path = models.TextField(null=True)
    # alttext = models.TextField(null=True)
    # mimetype = models.TextField(null=True)

class BulkProduct(Product):
    #id
    #name
    #category
    #price
    quantity = models.IntegerField()
    reorder_trigger = models.IntegerField()
    reorder_quantity = models.IntegerField();

class UniqueProduct(Product):
    #id
    #name
    #category
    #price
    serial_number = models.TextField()

class RentalProduct(Product):
    #id
    #name
    #category
    #price
    serial_number = models.TextField()

class ShoppingCartItem(models.Model):
    #id
    user = models.ForeignKey('account.FomoUser', related_name="user_shoppingcartitem")
    product = models.ForeignKey('Product')
    quantity = models.IntegerField()
    is_active = models.BooleanField(default=True)

    def calc_ext_amount(self):
        amount = self.product.price * self.quantity
        return amount

class ViewHistory(models.Model):
    #id
    user = models.ForeignKey('account.FomoUser')
    product = models.ForeignKey('Product')
    date_viewed = models.DateTimeField(auto_now_add=True)

class Sale(models.Model):
    #id
    date = models.DateTimeField(auto_now_add = True)
    subtotal = models.DecimalField(max_digits = 8, decimal_places = 2)
    total = models.DecimalField(max_digits = 8, decimal_places = 2)
    isFullyPaid =models.BooleanField(default = False)
    user = models.ForeignKey('account.FomoUser', on_delete = models.CASCADE)
    Address = models.TextField()
    City = models.TextField()
    State = models.TextField()
    Zip = models.TextField()
    def recordSale(self,User, Address, City, State, Zip, PaymentAmount):
        self.user = User
        self.Address = Address
        self.City = City
        self.State = State
        self.Zip = Zip
        self.subtotal = self.user.calc_subtotal()
        self.total = self.user.calc_total()
        self.save()
        items = ShoppingCartItem.objects.filter(user_id=self.user.id).filter(is_active=True)
        print('>>>>>>>>>>>>>>>im the items in the shopping cart nig', items)
        for i in items:
            product = i.product
            SI = SaleItem()
            SI.product = i.product
            SI.quantity = i.quantity
            SI.salePrice = i.calc_ext_amount()
            SI.sale = self
            if hasattr(product,'serial_number'):
                product.is_purchased = True
                product.save()
            else:
                product.quantity -= SI.quantity
            product.save()

            SI.save()
        self.subtotal = self.user.calc_subtotal()
        self.total = self.user.calc_total()
        p = Payment()
        p.amount = PaymentAmount
        p.sale = self
        self.isFullyPaid = True
        p.save()
        self.save()
    def get_tax(self):
        items = SaleItem.objects.filter(sale = self)
        sub = (self.subtotal + 10) *Decimal(.0725)
        sub = Decimal(round(sub,2))
        return sub

class SaleItem(models.Model):
    #id
    sale = models.ForeignKey('Sale')
    product = models.ForeignKey('Product')
    quanity = models.IntegerField(default=1)

class Payment(models.Model):
    amount = models.DecimalField(max_digits = 8, decimal_places = 2)
    sale = models.ForeignKey('Sale', on_delete=models.CASCADE)
