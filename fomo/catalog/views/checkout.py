from django.conf import settings
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from catalog import models as cmod
from formlib.form import FormMixIn
from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):
    if request.user.is_authenticated == False:
        return HttpResponseRedirect('/account/login/')
    if request.user.item_count() == 0:
        return HttpResponseRedirect('/catalog/index/')
    form = CheckoutForm(request)
    key = settings.STRIPE_PUBLIC_KEY
    return dmp_render(request,'checkout.html',{
    'form':form,
    'key':key,
    })

class CheckoutForm(FormMixIn, forms.Form):

    def init(self):
        self.fields['Cardname'] = forms.CharField(label="Cardholder Name", max_length=100)
        self.fields['CardNumber'] = forms.CharField(label="Card Number")
        self.fields['month']=forms.IntegerField(label="Expiration Month")
        self.fields['year']=forms.IntegerField(label="Expiration Year (YY)")
        self.fields['CVC']=forms.IntegerField(label="CVC")
