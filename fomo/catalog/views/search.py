from django.conf import settings
from django import forms
from django.http import HttpResponseRedirect
from django_mako_plus import view_function
from catalog import models as cmod
from .. import dmp_render
@view_function
def process_request(request):

    categories = cmod.Category.objects.order_by('id').all()


    instrument = request.POST.get('instrument')
    searchedProducts = cmod.Product.objects.all().filter(name__icontains=instrument)

    context = {
        'product': searchedProducts,
        'categories': categories
        }
    return dmp_render(request, 'search.html', context)
