from django.conf import settings
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from catalog import models as cmod
from .. import dmp_render, dmp_render_to_string
import stripe
@view_function
def process_request(request):
    if request.user.is_authenticated == False:
        return HttpResponseRedirect('/account/login/')

    # Set your secret key: remember to change this to your live secret key in production
    # See your keys here: https://dashboard.stripe.com/account/apikeys
    stripe.api_key = settings.STRIPE_SECRET_KEY
    # Token is created using Stripe.js or Checkout!
    # Get the payment token submitted by the form:
    token = request.POST['stripeToken'] # Using Flask
    # Charge the user's card:
    charge = stripe.Charge.create(
      amount=int(request.user.calc_total() * 100),
      currency="usd",
      description="Example charge",
      source=token,
    )
    print(charge)
    sale = cmod.Sale()
    sale.recordSale(request.user,request.user.address, request.user.city, request.user.state, request.user.postal, request.user.calc_total())
    request.user.clear_cart()
    id = sale.id
    return HttpResponseRedirect('/catalog/thanks/'+str(id))
