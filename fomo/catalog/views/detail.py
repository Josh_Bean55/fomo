from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from datetime import datetime
from catalog import models as cmod
from django import forms
from formlib.form import FormMixIn
from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):
    categories = cmod.Category.objects.order_by('id').all()

    pid = request.urlparams[0]
    try:
        product = cmod.Product.objects.get(id=pid)
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/index')

    if request.user.is_authenticated():
        viewhistory = cmod.ViewHistory()
        viewhistory.user = request.user
        viewhistory.product = product
        viewhistory.save()

    last5 = request.user.get_last_five()

    ##### Adding to cart ######
    form = AddToCartForm(request, product=product)
    if form.is_valid():
        form.commit()

    context = {
    'product': product,
    'product_images': cmod.ProductPicture.objects.filter(product_id=product.id),
    'categories': categories,
    'last5': last5,
    'form': form,
    }

    return dmp_render(request, 'detail.html', context)

class AddToCartForm(FormMixIn, forms.Form):
    form_submit = 'Add to Cart'
    form_id = 'add_to_cart_form'
    def init(self, product):
        if hasattr(product, 'quantity'):
            self.fields['quantity']= forms.IntegerField(required=False, max_value= product.quantity, min_value=1)
        self.user = self.request.user
        self.product = product

    def clean_quantity(self):
        qty = self.cleaned_data.get('quantity')
        if qty > self.product.quantity:
            raise forms.ValidationError('Try a lower quantity')
        if qty < 0:
            raise forms.ValidationError('Try a quantity greater than 0')
        return qty

    def commit(self):
        self.user.add_to_cart(self)


########################################################################################
@view_function
def get_quantity(request):
    #get the current quantity of product id in urlparams[0]
    try:
        product = cmod.BulkProduct.objects.get(id=request.urlparams[0])
    except (TypeError, cmod.BulkProduct.DoesNotExist):
        return HttpResponseRedirect('/catalog/index/')


    return HttpResponse(product.quantity)

#########################################################################################
