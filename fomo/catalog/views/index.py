from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required, permission_required
from django_mako_plus import view_function
from datetime import datetime
from catalog import models as cmod
from formlib.form import FormMixIn
from django import forms
from .. import dmp_render, dmp_render_to_string

@view_function
@login_required(login_url='/account/login/')

def process_request(request):
    #query all products
    products = cmod.Product.objects.filter(is_purchased=False).order_by('name').all()
    categories = cmod.Category.objects.order_by('id').all()
    print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Before if')

    if request.urlparams[0]:
        print('>>>>>>>>>>>>>>>>>>>>> After if', request.urlparams[0])
        products = cmod.Product.objects.all().filter(category=request.urlparams[0])

    last5 = request.user.get_last_five()
    print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>HEYY', last5)

    context = {
        'products': products,
        'categories': categories,
        'last5': last5,

    }
    return dmp_render(request, 'index.html', context )


########################################################################################
@view_function
def get_quantity(request):
    #get the current quantity of product id in urlparams[0]
    try:
        product = cmod.BulkProduct.objects.get(id=request.urlparams[0])
    except (TypeError, cmod.BulkProduct.DoesNotExist):
        return HttpResponseRedirect('/catalog/list/')


    return HttpResponse(product.quantity)
