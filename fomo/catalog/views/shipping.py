from django.conf import settings
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from catalog import models as cmod
from formlib.form import FormMixIn
from .. import dmp_render, dmp_render_to_string
import requests
import geocoder
@view_function
def process_request(request):
    if request.user.is_authenticated == False:
        return HttpResponseRedirect('/account/login/')
    if request.user.item_count() == 0:
        return HttpResponseRedirect('/catalog/index/')
    form = CheckoutForm(request)
    key = settings.STRIPE_PUBLIC_KEY
    useform = True
    worked = True
    address = ''
    if form.is_valid():
        ind =1
        address = form.cleaned_data.get('Address') + ', ' + form.cleaned_data.get('City') + ', ' + form.cleaned_data.get('State') + ' ' + form.cleaned_data.get('Zip')
        try:
            data = geocoder.google(address)
            request.user.address = data.housenumber + ' ' + data.street
            request.user.city = data.city
            request.user.state = data.state
            request.user.postal = data.postal
            request.user.save()
            address = data.housenumber+' '+data.street+', '+data.city+', '+data.state+' '+data.postal
        except:
            print('put code here')
            worked = False
        useform = False
        #return HttpResponse(data)
    #return HttpResponse(data)
    return dmp_render(request,'shipping.html',{
    'form':form,
    'useform':useform,
    'address':address,
    'worked':worked
    })

class CheckoutForm(FormMixIn, forms.Form):

    def init(self):
        self.fields['Address'] = forms.CharField(label="Address", max_length=100)
        self.fields['City'] = forms.CharField(label="City", max_length=100)
        self.fields['State']=forms.CharField(label="State", max_length=100)
        self.fields['Zip']=forms.CharField(label="Zip", max_length=100)
