from django.conf import settings
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from catalog import models as cmod
import sendgrid
import os
from sendgrid.helpers.mail import *
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
import stripe
from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):
    if request.user.is_authenticated == False:
        return HttpResponseRedirect('/account/login/')
    sale = cmod.Sale.objects.get(id = request.urlparams[0])

    send_mail(
        'Fomo-Receipt',
        'Thank you for shopping with FOMO, we always appreciate your business.',
        'fomo_admin@fomo.ltd',
        ['joshua.bean55@gmail.com'],
        fail_silently=False,
    )

    return dmp_render(request,'thanks.html',{
    'sale':sale,
    })
