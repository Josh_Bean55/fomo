from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from datetime import datetime
from catalog import models as cmod
from .. import dmp_render, dmp_render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required

@view_function
def process_request(request):
    #query all products
    products = cmod.ShoppingCartItem.objects.filter(user=request.user).filter(is_active=True).order_by('product').all()

    context = {
        'products': products,
    }
    return dmp_render(request, 'shoppingcart.html', context)


###########################################################################################################
#This is used to remove products from the shopping cart

@view_function
def delete(request):
    try:
        product = cmod.ShoppingCartItem.objects.get(id=request.urlparams[0])
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/shoppingcart/')

    product.is_active = False;
    product.save()

    return HttpResponseRedirect('/catalog/shoppingcart/')
