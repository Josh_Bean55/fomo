from django.test import TestCase
from catalog import models as cmod
from django.db import models
from polymorphic.models import PolymorphicModel
from decimal import Decimal


class CreateUniqueProductTestCase(TestCase):

    def test_create_a_unique_product(self):
        cat4 = cmod.Category()
        cat4.codename = "woodwind"
        cat4.name = "Woodwind Instruments"
        cat4.save()

        p1 = cmod.UniqueProduct()
        p1.name = 'Bassoon'
        p1.category = cat4
        p1.price = Decimal('20.00')
        p1.serial_number = '23514-MCBNST'
        p1.save()

        p2 = cmod.UniqueProduct.objects.get(id=p1.id)
        self.assertEquals(p2.name, 'Bassoon')
        self.assertEquals(p2.category, cat4)
        self.assertEquals(p2.price, Decimal('20.00'))
        self.assertEquals(p2.serial_number, '23514-MCBNST')
