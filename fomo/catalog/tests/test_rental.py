from django.test import TestCase
from catalog import models as cmod
from django.db import models
from polymorphic.models import PolymorphicModel
from decimal import Decimal

class CreateRentalProductTestCase(TestCase):


    def test_create_a_rental_product(self):
        cat3 = cmod.Category()
        cat3.codename = "brass"
        cat3.name = "Brass Instruments"
        cat3.save()

        p1 = cmod.RentalProduct()
        p1.name = 'Rental Tuba'
        p1.category = cat3
        p1.price = Decimal('80.00')
        p1.serial_number = "36874-BCZPO"
        p1.save()

        p2 = cmod.RentalProduct.objects.get(id=p1.id)
        self.assertEquals(p2.name, 'Rental Tuba')
        self.assertEquals(p2.category, cat3)
        self.assertEquals(p2.price, Decimal('80.00'))
        self.assertEquals(p2.serial_number, '36874-BCZPO')
