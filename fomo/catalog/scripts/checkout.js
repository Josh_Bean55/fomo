$(function() {

  var handler = StripeCheckout.configure({
    key: 'pk_test_6nBkaPwUi4r0Ic5AwdN5Zlwy',
    image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
    locale: 'auto',
    token: function(token) {
      $('#id_stripe_token').val(token.id);
    }
  });

  $('#checkout_form').submit(function(e) {
      if($('#id_stripe_token').val() != ''){
        return;
      }
    // Open Checkout with further options:
    handler.open({
      name: 'FOMO',
      description: '2 widgets',
      amount: 2000
    });
    e.preventDefault();
  });

  // Close Checkout on page navigation:
  window.addEventListener('popstate', function() {
    handler.close();
  });

});//ready
