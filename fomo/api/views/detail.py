from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from datetime import datetime
from catalog import models as cmod
from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):
    pid = request.urlparams[0]
    try:
        product = cmod.Product.objects.get(id=pid)
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/index')

    try:
        request.last5.remove(product.name)
    except ValueError:
        pass

    request.last5.insert(0, product.name)

    context = {
    'product': product,
    'product_images': cmod.ProductPicture.objects.filter(product_id=product.id)
    }

    return dmp_render(request, 'detail.html', context)


########################################################################################
@view_function
def get_quantity(request):
    #get the current quantity of product id in urlparams[0]
    try:
        product = cmod.BulkProduct.objects.get(id=request.urlparams[0])
    except (TypeError, cmod.BulkProduct.DoesNotExist):
        return HttpResponseRedirect('/catalog/index/')


    return HttpResponse(product.quantity)
