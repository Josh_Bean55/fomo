from django.conf import settings
from django import forms
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse
from .. import dmp_render, dmp_render_to_string
from django_mako_plus import view_function
from formlib.form import FormMixIn
from django.contrib.auth import authenticate, login
import json
from account import models as amod
from catalog import models as cmod

@view_function
def process_request(request):
    response = []
    if request.method=="GET":
        form = api_form(request.GET)
        if form.is_valid():
            products = cmod.Product.objects.all()
            if form.cleaned_data.get('product_name') !='':
                products = products.filter(name__icontains = form.cleaned_data.get('product_name'))
            if form.cleaned_data.get('min_price'):
                products = products.exclude(price__lt=form.cleaned_data.get('min_price'))
            if form.cleaned_data.get('category_name') !='':
                category = cmod.Category.objects.filter(name__icontains=form.cleaned_data.get('category_name'))
                products = products.filter(category_id__in= [o.id for o in category])
            if form.cleaned_data.get('max_price') != 0.0 :
                products = products.exclude(price__gt=form.cleaned_data.get('max_price'))
            for item in products:
                optional_category = 'quantity' if getattr(item,'quantity','serial_number') != 'serial_number' else 'serial_number'
                context ={
                'Name':item.name,
                'Category':item.category.name,
                optional_category:getattr(item,'quantity',getattr(item,'serial_number',0)),
                'Price':item.price,
                }
                response.append(context)
            return JsonResponse(response,safe=False)
        else:
            response.append(form.errors)
            return JsonResponse(response,safe=False)

class api_form(forms.Form):
    product_name  =     forms.CharField(required=False,initial='')
    min_price     =     forms.DecimalField(required=False,initial=0.0)
    max_price     =     forms.DecimalField(required=False,initial=0.0)
    category_name =     forms.CharField(required=False,initial='')

    def clean_min_price(self):
        min_price = self.cleaned_data.get('min_price')
        if  min_price is None:
            return 0.0
        return min_price

    def clean_max_price(self):
        max_price = self.cleaned_data.get('max_price')
        if  max_price is None:
            return 0.0
        return max_price
