from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from datetime import datetime
from catalog import models as cmod
from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):
    #query all products
    products = cmod.Product.objects.order_by('name').all()
    print('>>>>>>>', products)

    context = {
        'products': products,
    }
    return dmp_render(request, 'list.html', context)


########################################################################################
@view_function
def get_quantity(request):
    #get the current quantity of product id in urlparams[0]
    try:
        product = cmod.BulkProduct.objects.get(id=request.urlparams[0])
    except (TypeError, cmod.BulkProduct.DoesNotExist):
        return HttpResponseRedirect('/catalog/list/')


    return HttpResponse(product.quantity)
