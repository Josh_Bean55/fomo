from django.test import TestCase
from catalog import models as cmod
from django.db import models
from polymorphic.models import PolymorphicModel
from decimal import Decimal

class CreateBulkProductTestCase(TestCase):

    def test_create_a_bulk_product(self):
        cat5 = cmod.Category()
        cat5.codename = "accessory"
        cat5.name = "Music Accessories"
        cat5.save()

        p1 = cmod.BulkProduct()
        p1.name = 'Clarinet Reeds'
        p1.category = cat5
        p1.price = Decimal('2.50')
        p1.quantity = '10'
        p1.reorder_trigger = '25'
        p1.reorder_quantity = '100'
        p1.save()

        p2 = cmod.BulkProduct.objects.get(id=p1.id)
        self.assertEquals(p2.name, 'Clarinet Reeds')
        self.assertEquals(p2.category, cat5)
        self.assertEquals(p2.price, Decimal('2.50'))
        self.assertEquals(p2.quantity, 10)
        self.assertEquals(p2.reorder_trigger, 25)
        self.assertEquals(p2.reorder_quantity, 100)
