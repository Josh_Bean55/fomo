$(function() {

    var type = $('#id_type');

    type.change(function() {
        var value = type.val()
        if (value == 'bulk') {
            $('#id_create_date').closest('p').hide();
            $('#id_modified_date').closest('p').hide();
            $('#id_quantity').closest('p').show();
            $('#id_reorder_trigger').closest('p').show();
            $('#id_reorder_quantity').closest('p').show();
            $('#id_serial_number').closest('p').hide();
          }else if (value == 'rental')
          {
            $('#id_create_date').closest('p').hide();
            $('#id_modified_date').closest('p').hide();
            $('#id_quantity').closest('p').hide();
            $('#id_reorder_trigger').closest('p').hide();
            $('#id_reorder_quantity').closest('p').hide();
            $('#id_serial_number').closest('p').show();
          }else if (value == 'unique')
          {
            $('#id_create_date').closest('p').hide();
            $('#id_modified_date').closest('p').hide();
            $('#id_quantity').closest('p').hide();
            $('#id_reorder_trigger').closest('p').hide();
            $('#id_reorder_quantity').closest('p').hide();
            $('#id_serial_number').closest('p').show();
          }
      });
      type.change();
});
