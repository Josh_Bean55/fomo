$(function() {

    $('.delete_link2').click(function(event) {
      //cancel default behavior
      event.preventDefault();

      //open the modal
      $('#ConfirmModal2').modal({
        //no options
      })

      var href = $(this).attr('href');
      $('#really-delete-link2').attr('href', href);

    });//click
});
