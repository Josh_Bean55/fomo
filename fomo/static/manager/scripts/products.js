$(function() {

    $('.delete_link').click(function(event) {
      //cancel default behavior
      event.preventDefault();

      //open the modal
      $('#ConfirmModal').modal({
        //no options
      })

      var href = $(this).attr('href');
      $('#really-delete-link').attr('href', href);

    });//click

    $('.update_quantity_button').click(function() {
      //build the url
      var button = $(this);
      var url = '/manager/products.get_quantity/' + button.attr('data-pid');

      //call ajax
      //button.siblings('.quantity_test').load(url);
      button.siblings('.quantity_text').load(url);

    }); //click

});
